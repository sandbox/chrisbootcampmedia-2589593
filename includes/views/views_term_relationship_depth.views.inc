<?php

/*
 * implements hook_field_views_data_views_data_alter().
 */

function views_term_relationship_depth_views_data_alter(&$data) {
 foreach (field_info_fields() as $field) {
    if ($field['storage']['type'] != 'field_sql_storage') {
      continue;
    }
    if ($field['type'] != 'taxonomy_term_reference') {
      continue;
    }
    foreach ($field['bundles'] as $entity_type => $bundles) {
      $entity_info = entity_get_info($entity_type);
      $pseudo_field_name = 'reverse_depth_' . $field['field_name'] . '_' . $entity_type;

      list($label, $all_labels) = field_views_field_label($field['field_name']);
      $entity = $entity_info['label'];
      if ($entity == t('Node')) {
        $entity = t('Content');
      }

      $data['taxonomy_term_data'][$pseudo_field_name]['relationship'] = array(
        'title' => t('@entity using @field with depth', array('@entity' => $entity, '@field' => $label)),
        'help' => t('Relate each @entity with a @field set to a descendent of the term.', array('@entity' => $entity, '@field' => $label)),
        'handler' => 'views_handler_relationship_term_with_depth',
        'field_name' => $field['field_name'],
        'field table' => _field_sql_storage_tablename($field),
        'field field' => $field['field_name'] . '_tid',
        'base' => $entity_info['base table'],
        'base field' => $entity_info['entity keys']['id'],
        'label' => t('!field_name', array('!field_name' => $field['field_name'])),
        'join_extra' => array(
          0 => array(
            'field' => 'entity_type',
            'value' => $entity_type,
          ),
          1 => array(
            'field' => 'deleted',
            'value' => 0,
            'numeric' => TRUE,
          ),
        ),
      );
    }
  }
}

