<?php

class views_handler_relationship_term_with_depth  extends views_handler_relationship  {
  function init(&$view, &$options) {
    parent::init($view, $options);

    $this->field_info = field_info_field($this->definition['field_name']);
  }

  /**
   * Called to implement a relationship in a query.
   */
  function query() {
    $this->ensure_my_table();
    $max_depth = $this->options['max_depth'];
    // First, relate our base table to the current base table to the
    // field, using the base table's id field to the field's column.
    $views_data = views_fetch_data($this->table);
    $left_field = $views_data['table']['base']['field'];

    $member_table = $this->definition['field table'];
    $member_term_field = $this->definition['field field'];
    $member_entity_field = 'entity_id';
    
    $members = db_select($member_table,'m');
    $members->addField('m',$member_term_field,'term_id');
    $members->addField('m',$member_entity_field);
    
    $result = $members;
    
    for ($depth = 1;$depth<=$max_depth;$depth++) {
      $members = db_select($member_table,'m');
      $join_term = 'm.'.$member_term_field;
      for ($i=1; $i<=$depth; $i++) {
        $alias = 'h'.$i;
        $members->join('taxonomy_term_hierarchy',$alias,"$join_term = $alias.tid");
        $join_term = $alias.'.parent';
      }
      $members->addField($alias,'parent','term_id');
      $members->addField('m',$member_entity_field);
      $result = $result->union($members);
    }
            
            
    $first = array(
      'left_table' => $this->table_alias,
      'left_field' => $left_field,
      'table' => $result,
      'field' => 'term_id',
    );
    if (!empty($this->options['required'])) {
      $first['type'] = 'INNER';
    }


    if (!empty($this->definition['join_handler']) && class_exists($this->definition['join_handler'])) {
      $first_join = new $this->definition['join_handler'];
    }
    else {
      $first_join = new views_join();
    }
    $first_join->definition = $first;
    $first_join->construct();
    $first_join->adjusted = TRUE;

    $this->first_alias = $this->query->add_table('hm', $this->relationship, $first_join);

    // Second, relate the field table to the entity specified using
    // the entity id on the field table and the entity's id field.
    $second = array(
      'left_table' => $this->first_alias,
      'left_field' => $member_entity_field,
      'table' => $this->definition['base'],
      'field' => $this->definition['base field'],
    );

    if (!empty($this->options['required'])) {
      $second['type'] = 'INNER';
    }

    if (!empty($this->definition['join_handler']) && class_exists($this->definition['join_handler'])) {
      $second_join = new $this->definition['join_handler'];
    }
    else {
      $second_join = new views_join();
    }
    $second_join->definition = $second;
    $second_join->construct();
    $second_join->adjusted = TRUE;

    // use a short alias for this:
    $alias = $this->definition['field table'];

    $this->alias = $this->query->add_relationship($alias, $second_join, $this->definition['base'], $this->relationship);
  }
  
  function option_definition() {
    $options = parent::option_definition();
    $options['max_depth'] = array('default' => 1, 'int' => TRUE);

    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['max_depth'] = array(
      '#title' => t('Maximum depth'),
      '#description' => t('Number of steps of ancestry. Large numbers may cause performance issues'),
      '#type' => 'textfield',
      '#default_value' => $this->options['max_depth'],
      '#required' => true,
      '#maxlength'  => 1,
      '#size' => 1,
      
    );
  }
  
  function options_validate(&$form, &$form_state) { 
    parent::options_validate($form, $form_state);
    $max_depth = $form_state['values']['options']['max_depth'];
    if (!is_numeric($max_depth) || $max_depth<0 ) {
      form_set_error('max_depth', t('postitive integer required '));
    }
    $form_state['values']['options']['max_depth'] = intval($max_depth);
  }


}